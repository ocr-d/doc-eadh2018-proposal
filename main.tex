\documentclass{article}
\usepackage[utf8]{inputenc}

\title{Towards Context-Aware Language Models for Historical OCR Post-Correction}
\author{}
%\date{December 7, 2018}
\date{\today}

\usepackage{natbib}
%\usepackage{graphicx}

\begin{document}

\maketitle

\section{Introduction}

While the last decade brought substantial improvements in OCR, historic material is still challenging: special orthography, vocabulary, symbols, fonts, and layout, and sometimes poor visual quality hamper large-scale full-text digitisation. 
To account for these factors, on the language processing side of the problem, stochastic models need not only text samples but information %identifying 
which contexts they are representative of.

Owing to the vast diversity and complexity of language, context can span multiple dimensions at different scales: from global ``domain'' attributes like document type, genre, time, and origin to local ``markup'' information such as region class, font, form features, and language. Ideally, OCR should condition its approximation model on these meta-data as much as possible. 

However, in practice such systems are already quite complex, meta-data schemes are very diverse, % inhomogeneous? vary a lot?
and historical ground truth corpora are still scarce \citep{springmann2018}. 
So instead of extending existing OCR, it can be prudent to first build complementary tools for post-correction which feed from % all visual and contextual cues of OCR itself, 
the OCR's search space, but incorporate more linguistic knowledge, and maintain their own comprehensive models. % mention image processing focus of OCR vs linguistic knowledge focus of post-correction? \citep{schulzkuhn2017}

This project is part of the joint coordinated research effort OCR-D \citep{ocrd2018}, which aims at developing OCR methods for historic material in preparation of automated mass-digitisation of all German-speaking prints from the 16--19th century. 
Our module is dedicated to post-correction. We shall first explore various techniques among the major modelling paradigms of weighted finite-state transducers (WFST) and artificial neural networks (ANN). Based on these experiments, a system will be implemented that can be executed within the OCR-D pipeline, or run standalone. We shall provide standard models which exploit all available data and meta-data for both historic and contemporary texts, as well as training tools.

\section{Methodology} % or Approach?

Many existing post-correction approaches employ WFST \citep{tongevans1996,brillmoore2000,kolak2005,llobet2010,alazawibreuel2014,reffleringlstetter2012}, which allow for compact storage and processing of symbol sequences. Using prior knowledge on what mistakes OCR typically makes, how new words are formed grammatically, and which words are likely to appear next to each other, a  post-correction system can be modelled as the \emph{composition} of single transducers representing input character hypotheses, error model, lexicon, and word-level language model, each weighted stochastically. The result is a transducer containing all possible correction candidates alongside their probabilities.

In synthetic languages, a mere list of word forms does not suffice as word model. We will therefore extend the lexicon to a \emph{dynamic} word model by using a morphology transducer.

One of the most common errors in OCR is word segmentation. Hence, post-correction should search across multiple words at once. Since the WFST approach is unable to handle long inputs efficiently, we will implement a \emph{sliding window} technique, recombining window outputs afterwards. 
% mention line boundaries and punctuation as common sources of error?

Recent advances in deep-learning methods and their success in diverse applications have spurred researchers on proposing new post-correction approaches based on ANN \citep{alazawi2014,dhondt2017,dongsmith2018}. Neural networks offer more efficient predictions,
and are more powerful, while offering very good practical generalisation performance (the causes of which are slowly becoming understood  \citep{geman1992,barron1994,bengio1994,lee2017,zhang2017,advanisaxe2017}).

Of particular attraction (especially in machine translation) is the \emph{encoder-decoder} model for sequence-to-sequence transduction \citep{cho2014,sutskever2014} (often augmented with attention \citep{bahdanau2014,vaswani2017}, sometimes protected against exposure bias \citep{wisemanrush2016,lamb2016,huszar2015,bengio2015}).
%As with language modelling, it can be applied on the level of words or characters. 
%Both can be combined in many ways, implicitly \citep{chung2016} and explicitly \citep{kim2015,luongmanning2016}, or even successively (by using a word level model to rescore the search space of a character level model).
%For historic texts, with frequent use of Greek and Hebrew scripts, and a large number of rare graphemes, descending to the \emph{byte level} of a UTF-8 transport stream is both more reliable, and helps to keep models compact \citep{gillick2015}.

For estimation of the OCR error model, most WFST approaches use supervised learning,
%(exploiting manually corrected data, which is not only too expensive but also hard to acquire in a way representative of all contexts)
but unsupervised learning via Expectation Maximisation has been proven effective as well – both inductively \citep{tongevans1996} and transductively \citep{reffleringlstetter2012}. However, the data in all available contexts still must be split into a finite number of discrete, static \emph{domains}, which arguably is an ill-defined, reductionist notion \citep{ruder2016}. 

With ANN, since models can be pre-trained on auxiliary tasks and then transferred as initializer or fixed parameter sub-space \citep{bengio2006, howardruder2018}, %or optimising multiple related tasks\citep{ruder2017}
it is easy to learn a single inductive model both on (scarce) labelled and (plenty) unlabelled data. Moreover, this model can be made \emph{context-aware} by feeding all context variables as extra input and having the network learn an implicit lower-dimensional \emph{embedding}, which dynamically combines with all components and thus makes them adaptive \citep{jaechostendorf2017,yogatama2017}. 

Applied to the encoder-decoder post-correction model,
%which is trained to reduce the reconstruction loss (effectively learning an error model in the encoder and a conditional language model in the decoder),
this means we can perform supervised training by presenting OCR output to the encoder and ground truth text to the decoder, while also doing \emph{unsupervised training} by presenting (both historic and contemporary)
\begin{itemize}
\item clean texts to both sides, 
\item noisy OCR texts to both sides, and
\item clean text to the decoder, but randomly degraded text to the encoder \citep{dhondt2017}.
\end{itemize}
%In all cases the encoder input side can be made to resemble OCR output either as single-best results or confidence-backed character hypotheses. 
Context variables could be input to both encoder and decoder, both hidden and output layers, both additively and multiplicatively.
%Additionally, in order to make maximal use of these metadata, some of which we a priori know to be drawn from non-stationary processes of language change, e.g.\ time and local origin, the loss can be complemented by constraints on smoothness and directedness of context embeddings \citep{hamilton2016}. 

\section{Experiments} % really?

Early evaluation of our %WFST and ANN 
prototypes was done on \emph{GT4HistOCR}'s \texttt{dta19} sub-corpus \citep{springmann2018}. Measuring CER and precision-recall \citep{reynaert2008}, we find that 
\begin{itemize}
    \item character-level ANN outperform word-based WFST even when providing an oracle lexicon,
    % \item mixed error models are only beneficial when training data are scarce,
    % \item hybrid ANN-WFST combination without alternative input hypotheses does not help,
    \item ANN lean towards precision, whereas WFST allow some %level of
    control over precision vs recall.
\end{itemize}
% prototypes FST-based (figure) ANN-based (figure)
% first results (figures or tables?)
% next steps


\bibliographystyle{plain}
\bibliography{references}
\end{document}
